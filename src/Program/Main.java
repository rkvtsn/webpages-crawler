package Program;

import jCrawler.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;



public class Main {

    static String hostname1 = "jdbc:postgresql://localhost:5432/webdb_relatives";
    static String hostname2 = "jdbc:postgresql://localhost:5432/testbase";
    static String username = "postgres";
    static String password = "123456";
    static Connection webdb = null;
    static Connection testdb = null;



    public static void main(String[] args) throws Exception {
        Connect();
        new ProxyConnection("10.128.0.100", "8080");

        // TODO: @CrawlerApp-> add checking for @List on existence in @TEMP

        crawler().launch(2);
////        crawler("whitePages").launch(0);
////        crawler("adults").launch(0);
////        crawler("violence").launch(0);
        webdb.close();
        testdb.close();

    }


    static private IApp crawler(String category) throws SQLException, FileNotFoundException {
        List<String> links = getLinksFromTxt("D:\\Projects\\ParseDb\\resources\\urls\\" + category + ".txt");
        return new CrawlerByList(webdb, links, category);
    }

    static private IApp crawler() throws SQLException {
        return new AppFromAnotherDb(webdb, testdb, " SELECT * FROM htmlcode ", "url", "code", "type");
    }


    static void Connect() throws Exception {
        Class.forName("org.postgresql.Driver");
        webdb = DriverManager.getConnection(hostname1, username, password);
        testdb = DriverManager.getConnection(hostname2, username, password);
    }

    static List<String> getLinksFromTxt(String fileName) throws FileNotFoundException {
        List<String> result = new ArrayList<String>();

        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Lines in the file (" + fileName + "): " + result.size());

        return result;
    }


    static private void domainTest (List<String> links) {

        links.add("absolute.com/index.php?page=info");
        links.add("/relative/index.php?page=info");
        links.add("index.php?page=info");
        links.add("/index.php?page=info");
        links.add("index.php/?page=info");
        links.add("/index.php/?page=info");
        links.add("index.php?page=info");

        for (String link : links) {
            link = Page.prepareLink(link);
            System.out.println(Page.getDomain(link));
        }

    }

    static private void absUrlTest (List<String> links, String parentLink) throws MalformedURLException {

        links.add("absolute.com/index.php?page=info");
        links.add("/relative/index.php?page=info");
        links.add("index.php?page=info");
        links.add("/index.php?page=info");
        links.add("index.php/?page=info");
        links.add("/index.php/?page=info");
        links.add("index.php?page=info");

        for (String link : links) {
            System.out.println(Page.getAbsLink(link, parentLink));
        }

    }



}
//
// new AppFromAnotherDb(webdb, testdb, "SELECT * FROM htmlcode LIMIT 1", "url", "code");
// new AppRetry(webdb);
// new AppFromTxt(webdb, "file.txt");
//IApp crawler = new AppFromAnotherDb(webdb, testdb, "SELECT * FROM htmlcode LIMIT 1", "url", "code");
//  IApp crawler = new CrawlerByList(webdb, links, "portal");
///
//        List<String> links =  new ArrayList<String>();
//        links.add("www.yandex.ru");

