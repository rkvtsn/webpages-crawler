package Program;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class PageOld {

    public int id;
    public int domain_id;
    public String domain;

    public String page_url;

    public String html_code;
    public String html_text;

    public Document doc = null;


    public PageOld(String url) throws Exception {
        setDomain(url);
        tryGetByHttp(url);
    }

    public PageOld(String url, String html_code) throws Exception {
        setDomain(url);
        doc = Jsoup.parse(html_code);
    }

//    // for Update
//    public Page(String url, int id) throws Exception {
//        this.id = id;
//        this.page_url = url;
//        setDomain(url);
//    }


    private void tryGetByHttp(String url) throws InterruptedException, IOException, SQLException {
        if (domain_id == -1) { doc = null; return; }

        Connection con = Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                .referrer("http://www.google.com")
                //.cookie("TCK", "e2b1204bfde3e72b37fe31317ada2ef1")
                .ignoreHttpErrors(true)
                .followRedirects(true)
                .timeout(5000);

        Connection.Response resp = null;
        Thread.sleep(400); // without can be overflow
        try {
            resp = con.execute();
        } catch (Exception e) {
            bad_request(-1, url); // unreachable or unknown errors
        }

        //todo redirects
        while (resp.statusCode() == 307) {
            String urlRedir = resp.header("Location");
            con = Jsoup.connect(urlRedir)
                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                    .referrer("http://www.google.com")
                    .ignoreHttpErrors(true)
                    .followRedirects(true)
                    .timeout(5000);
        }

        if (resp.statusCode() == 200) {
            doc = con.get();
        } else {
            bad_request(resp.statusCode(), url); // typical http errors + redirects
        }
    }

    private void bad_request(int statusCode, String url) throws SQLException {
        doc = null;
        PreparedStatement st = Main.webdb.prepareStatement("INSERT INTO pages " +
                " (url, domain_id, error) " +
                " VALUES (?, ?, ?) "); // todo static
        st.setString(1, url);
        st.setInt(2, domain_id);
        st.setInt(3, statusCode);
        st.executeUpdate();
    }


    protected void parse_page() throws SQLException, MalformedURLException, URISyntaxException {
        html_code = doc.html();
        html_text = doc.text();

        String title  = doc.getElementsByTag("title").text();
        String meta_keywords = doc.select("meta[name=keywords]").attr("content");
        String meta_description  = doc.getElementsByTag("meta[name=description]").attr("content");
        String p_text = doc.select("p").text();

        // .content
        String content = doc.select("div.content, div#content, article, section").text();
        // .header
        String header = doc.select("#header, .header, header").text();
        // .footer
        String footer = doc.select("#footer, .footer, footer").text();

        PreparedStatement insertPageStmt = Main.webdb.prepareStatement("INSERT INTO pages " +
                "(html_code, html_text, title, meta_keywords, meta_description, p_text, content, header, footer, error, url, domain_id) " +
                " VALUES " +
                "(?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS); // todo static

        insertPageStmt.setString(1, html_code);
        insertPageStmt.setString(2, html_text);
        insertPageStmt.setString(3, title);
        insertPageStmt.setString(4, meta_keywords);
        insertPageStmt.setString(5, meta_description);
        insertPageStmt.setString(6, p_text);
        insertPageStmt.setString(7, content);
        insertPageStmt.setString(8, header);
        insertPageStmt.setString(9, footer);
        insertPageStmt.setInt(10, 0); // 0 : no errors
        insertPageStmt.setString(11, page_url);
        insertPageStmt.setInt(12, domain_id);

        insertPageStmt.executeUpdate();

        ResultSet set = insertPageStmt.getGeneratedKeys();
        set.next();
        id = set.getInt(1);

        Main.webdb.setAutoCommit(false);
        // em, i; cite; b, strong; u; quote; del, strike; mark; dfn
        insertTexts(doc.select("cite, quote, del, strike, mark, dfn"), "tag_decoration");
        insertTexts(doc.getElementsByTag("u"), "tag_u");
        insertTexts(doc.select("em, i"), "tag_italic");
        insertTexts(doc.select("b, strong"), "tag_bold");
        // blocks
        insertTexts(doc.getElementsByTag("div"), "tag_div");
        insertTexts(doc.getElementsByTag("span"), "tag_span");
        // h*
        insertTexts(doc.getElementsByTag("h1"), "tag_h1");
        insertTexts(doc.getElementsByTag("h2"), "tag_h2");
        insertTexts(doc.getElementsByTag("h3"), "tag_h3");
        // navigation-elements
        insertLinks(doc.select(".nav a, #nav a, #menu a, .menu a, menu a, ul a"), "tag_nav");
        // images
        Elements images = doc.getElementsByTag("img");
        Statement st = Main.webdb.createStatement(); // todo prepare static
        for(Element img : images) {
            String query = "INSERT INTO tag_img (src, alt, page_id) " +
                    "VALUES ( '" + img.attr("src") + "', '" + img.attr("alt") + "', "+ this.id +")";
            st.addBatch(query);
        }
        st.executeBatch();
        Main.webdb.setAutoCommit(true);
    }






    public List<String> proc() throws SQLException, URISyntaxException, MalformedURLException {
        if (doc == null) return new ArrayList<String>();
        parse_page();
        return insertLinks(doc.getElementsByTag("a"));

    }

    public void simple_proc() throws URISyntaxException, SQLException, MalformedURLException {
        if (doc == null) return;
        parse_page();
        insertLinks(doc.getElementsByTag("a"), "tag_a");
    }

    public void simple_proc(int parent_id) throws URISyntaxException, SQLException, MalformedURLException {
        if (doc == null) return;

        parse_page();

        // then add parent-child
        try {
            PreparedStatement insertIntoRelatives = Main.webdb.prepareStatement("INSERT INTO relatives " +
                    "(parent_id, child_id) VALUES (?,?)"); // todo static
            insertIntoRelatives.setInt(1, parent_id);
            insertIntoRelatives.setInt(2, this.id);
            insertIntoRelatives.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage() + " \n "  +
                    e.getNextException());
        }
        insertLinks(doc.getElementsByTag("a"), "tag_a");
    }

    public List<String> proc(int parent_id) throws SQLException, MalformedURLException, URISyntaxException {
        if (doc == null) return new ArrayList<String>();

        parse_page();

        // then add parent-child
        try {
            PreparedStatement insertIntoRelatives = Main.webdb.prepareStatement("INSERT INTO relatives " +
                    "(parent_id, child_id) VALUES (?,?)"); // todo static
            insertIntoRelatives.setInt(1, parent_id);
            insertIntoRelatives.setInt(2, this.id);
            insertIntoRelatives.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage() + " \n "  +
                    e.getNextException());
        }
        return insertLinks(doc.getElementsByTag("a"));
    }



    void insertLinks(Elements links, String table) throws SQLException, URISyntaxException, MalformedURLException {
        PreparedStatement st = Main.webdb.prepareStatement(
                " INSERT INTO " + table + " (value, href, page_id) " +
                " VALUES ( ?, ?, ?)");
        for (Element link : links) {
            String href = link.attr("href");
            st.setString(1, link.text());
            st.setString(2, href);
            st.setInt(3, this.id);
            st.addBatch();
        }
        st.executeBatch();
        st.clearBatch();
    }

    List<String> insertLinks(Elements links) throws SQLException, URISyntaxException, MalformedURLException {
        List<String> result = new ArrayList<String>();
        PreparedStatement st = Main.webdb.prepareStatement(
                " INSERT INTO tag_a (value, href, page_id) " +
                        " VALUES ( ?, ?, ?)");
        for (Element link : links) {
            String href = link.attr("href");
            st.setString(1, link.text());
            st.setString(2, href);
            st.setInt(3, this.id);
            st.addBatch();

            addLinkTo(href, result);
        }
        st.executeBatch();
        st.clearBatch();
        return result;
    }


    void addLinkTo(String link, List<String> result) throws URISyntaxException, MalformedURLException {

        if (link == null) return;
        link = link
                .toLowerCase()
                .trim();
        // simple filter
        if (link.equals("") || link.equals(page_url) || link.contains("javascript:") || link.contains("mailto:")) return;

        // filter on spaces in url
        link = link.replace(" ","%20");

        // hash filter
        int hashIndex = link.indexOf("#");
        if (hashIndex != -1) {
            link = link.substring(0, hashIndex);
        }

        String link_domain = null;
        try {
            URL link_url = new URL(link);
            link_domain = (link_url).getHost();
        } catch (IOException e) {  }

        if (link_domain == null) {
            // if relative url
            URL realUrl = new URL(new URL(page_url), link);
            // make absolute url
            link = realUrl.toString();
        }

        result.add(link);
    }

    void insertTexts(Elements texts, String table) throws SQLException {
        PreparedStatement st = Main.webdb.prepareStatement(
                " INSERT INTO " + table + " (value, page_id) " +
                " VALUES ( ?, ?)");
        for (Element t : texts) {
            st.setString(1, t.text());
            st.setInt(2, this.id);
            st.addBatch();
        }
        try {
            st.executeBatch();
            st.clearBatch();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }


    private void setDomain(String url) throws Exception {
        domain_id = -1;
        this.page_url = url;
        URI uri = new URI(url.toLowerCase().trim());

        domain = uri.getHost();
        if (domain == null) throw new Exception("Error - wrong url");

        domain = domain.startsWith("www.") ? domain.substring(4) : domain;

        Statement st = Main.webdb.createStatement();
        ResultSet rs = st.executeQuery("SELECT id FROM domains WHERE url = '" + domain + "'"); // todo static

        if (!rs.next()) {
            st.execute("INSERT INTO domains (url) VALUES ('"+domain+"')", Statement.RETURN_GENERATED_KEYS); // todo static
            ResultSet set = st.getGeneratedKeys();
            set.next();
            domain_id = set.getInt(1);
        } else {
            domain_id = rs.getInt("id");
        }

        if (domain_id == -1) throw new Exception("Error domain.id != -1");
    }

}
