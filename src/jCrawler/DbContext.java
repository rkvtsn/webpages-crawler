package jCrawler;


import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

final public class DbContext {

    private final Connection connection;

    private final PreparedStatement selectCategoryId;
    private final PreparedStatement insertPageCategory;
    private final PreparedStatement selectDomainId;
    private final PreparedStatement domainInsert;

    /***
     * @return <strong>Scrollable Result Set</strong>
     */
    public final Statement getStmt() { return sql; }
    private final Statement sql;

    public final PreparedStatement getParseUpdate() { return parseUpdate; }
    public final PreparedStatement getParseInsert() { return parseInsert; }

    private final PreparedStatement parseUpdate;
    private final PreparedStatement parseInsert;

    private final PreparedStatement insertCategory;

    private final PreparedStatement insertRelative;
    private final PreparedStatement insertImage;

    private final PreparedStatement insertTempPage;
    private final PreparedStatement updateTempPage;
    private final PreparedStatement insertNavLink;
    private final PreparedStatement insertLink;

    public void setAutoCommit(boolean state) throws SQLException {
        this.connection.setAutoCommit(state);
    }

    public DbContext(java.sql.Connection webDb) throws SQLException {
        this.connection = webDb;

        parseInsert = connection.prepareStatement("INSERT INTO pages " +
                "(html_code, html_text, title, meta_keywords, meta_description, p_text, content, header, footer, error, url, domain_id) " +
                " VALUES " +
                "(?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
        parseUpdate = connection.prepareStatement("UPDATE pages " +
                " SET html_code = ?, html_text = ?, title = ?, meta_keywords = ?, meta_description = ?, p_text = ?, content = ?, header = ?, footer = ?, error = ?, url = ?, domain_id = ? " +
                " WHERE id = ? ");

        // #temp page
        insertTempPage = connection.prepareStatement("INSERT INTO pages " +
                "(url, error)" +
                " VALUES " +
                "(?,?)", Statement.RETURN_GENERATED_KEYS);

        updateTempPage = connection.prepareStatement("UPDATE pages " +
                " SET error = ? WHERE id = ? ");


        sql = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
            ResultSet.CONCUR_READ_ONLY,
            ResultSet.HOLD_CURSORS_OVER_COMMIT); //ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY

        insertRelative = connection.prepareStatement("INSERT INTO relatives " +
                "(parent_id, child_id) VALUES (?,?)");

        insertImage = connection.prepareStatement( "INSERT INTO tag_img (src, alt, page_id) " +
                "VALUES ( ?, ?, ?)");

        insertNavLink = connection.prepareStatement(
                " INSERT INTO tag_nav (value, href, page_id) " +
                        " VALUES ( ?, ?, ?)");

        insertLink = connection.prepareStatement(
                " INSERT INTO tag_a (value, href, page_id) " +
                        " VALUES ( ?, ?, ?)");

        // #domain
        domainInsert = connection.prepareStatement("INSERT INTO domains (url) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
        selectDomainId = connection.prepareStatement("SELECT id FROM domains WHERE url = ? ");

        // #category
        insertCategory = connection.prepareStatement("INSERT INTO categories (title) VALUES(?) ", Statement.RETURN_GENERATED_KEYS);
        selectCategoryId = connection.prepareStatement("SELECT id FROM categories WHERE title = ? ");
        insertPageCategory = connection.prepareStatement("INSERT INTO category_page (category_id, page_id, score) " +
                "VALUES (?, ?, ?)");
    }

    public void insertTempPages(List<String> links, int statusCode) throws SQLException {
        connection.setAutoCommit(false);
        insertTempPage.clearBatch();
        for (String link : links) {
            insertTempPage.setString(1, link);
            insertTempPage.setInt(2, statusCode);
            insertTempPage.addBatch();
        }
        try {
            insertTempPage.executeBatch();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage() + " \n "  + e.getNextException());
        }
        insertTempPage.clearBatch();
        connection.setAutoCommit(true);
    }

    public void insertTempPage(String url, int statusCode) throws SQLException {
        try {
            insertTempPage.setString(1, url);
            insertTempPage.setInt(2, statusCode);
            insertTempPage.executeUpdate();
            insertTempPage.clearParameters();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage() + " \n "  + e.getNextException());
        }
    }


    public void updateTempPage(int id, int statusCode) throws SQLException {
        updateTempPage.setInt(2, id);
        updateTempPage.setInt(1, statusCode);
        try {
            updateTempPage.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage() + " \n "  + e.getNextException());
        }
        updateTempPage.clearParameters();
    }

    public ResultSet selectWhereError(String sign, int error) throws SQLException {

        Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
            ResultSet.CONCUR_READ_ONLY,
            ResultSet.HOLD_CURSORS_OVER_COMMIT);

        return stmt.executeQuery("SELECT pages.id id, url, categories.title category, score FROM pages " +
                                " LEFT OUTER JOIN category_page ON pages.id = category_page.page_id " +
                                " LEFT OUTER JOIN categories ON category_id = categories.id " +
                                " WHERE error " + sign + " " + error);
    }

    public ResultSet selectWhereError(int error) throws SQLException {
        return selectWhereError("=", error);
    }

    public void insertChild(int id, int parentId) throws SQLException {
        if (id == -1) return;
        try {
            insertRelative.setInt(1, parentId);
            insertRelative.setInt(2, id);
            insertRelative.executeUpdate();
            insertRelative.clearParameters();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage() + " \n "  + e.getNextException());
        }
    }


    public void insertTexts(Elements texts, String table, int pageId) throws SQLException {
        PreparedStatement st = connection.prepareStatement(
                " INSERT INTO " + table + " (value, page_id) " +
                        " VALUES ( ?, ?)");
        for (Element t : texts) {
            st.setString(1, t.text());
            st.setInt(2, pageId);
            st.addBatch();
        }
        try {
            st.executeBatch();
            st.clearBatch();
            st.close();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }


    public void insertImage(String src, int pageId) throws SQLException {
        insertImage.setString(1, src);
        insertImage.setString(2, "");
        insertImage.setInt(3, pageId);
        insertImage.addBatch();
        try {
            insertImage.execute();
            insertImage.clearParameters();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }

    public void insertImages(Elements images, int pageId) throws SQLException {
        for (Element img : images) {
            insertImage.setString(1, img.attr("src"));
            insertImage.setString(2, img.attr("alt"));
            insertImage.setInt(3, pageId);
            insertImage.addBatch();
        }
        try {
            insertImage.executeBatch();
            insertImage.clearParameters();
            insertImage.clearBatch();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }

    void insertNavLinks(Elements links, int pageId) throws SQLException {
        for (Element link : links) {
            insertNavLink.setString(1, link.text());
            insertNavLink.setString(2, link.attr("href"));
            insertNavLink.setInt(3, pageId);
            insertNavLink.addBatch();
        }
        try {
            insertNavLink.executeBatch();
            insertNavLink.clearParameters();
            insertNavLink.clearBatch();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }

    public List<String> insertLinks(Elements links, int pageId) throws SQLException {
        List<String> result = new ArrayList<String>();

        for (Element link : links) {
            result.add(onInsertLinkAdd(link, pageId));
        }
        try {
            insertLink.executeBatch();
            insertLink.clearParameters();
            insertLink.clearBatch();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
        return result;
    }

    public String onInsertLinkAdd(Element link, int pageId) throws SQLException {

        String href = link.attr("href");
        insertLink.setString(1, link.text());
        insertLink.setString(2, href);
        insertLink.setInt(3, pageId);
        insertLink.addBatch();

        return href;
    }
    public void onInsertLinkExecute() throws SQLException {
        try {
            insertLink.executeBatch();
            insertLink.clearParameters();
            insertLink.clearBatch();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }

    public void insertPageCategory(String categoryName, int pageId, double score) throws SQLException {
        try {
            int categoryId = getCategoryId(categoryName);
            insertPageCategory.setInt(1, categoryId);
            insertPageCategory.setInt(2, pageId);
            insertPageCategory.setDouble(3, score);
            insertPageCategory.executeUpdate();
            insertPageCategory.clearParameters();
        }catch(SQLException e) {
            throw new SQLException(e.getMessage() + " \n " + e.getNextException());
        }
    }

    public int getCategoryId(String categoryName) throws SQLException {
        int categoryId;
        selectCategoryId.setString(1, categoryName);
        ResultSet rs = selectCategoryId.executeQuery();

        if (!rs.next()) {
            insertCategory.setString(1, categoryName);
            insertCategory.executeUpdate();
            ResultSet set = insertCategory.getGeneratedKeys();
            set.next();
            categoryId = set.getInt(1);
        } else {
            categoryId = rs.getInt("id");
        }
        return categoryId;
    }


    public static final class ERROR {
        static public final int TEMP = 0;
        static public final int BLOCKED = 1;
        static public final int UNKNOWN = 2;
        static public final int OK = 200;
    }

}
