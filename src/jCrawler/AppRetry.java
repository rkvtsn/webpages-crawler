package jCrawler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class AppRetry extends App {

    public AppRetry(Connection webDb) throws SQLException, IOException {
        super(webDb);
        resultSet = ctx.selectWhereError("<>", DbContext.ERROR.OK);
    }

    // TODO category
    @Override
    protected Page getRootPage() throws Exception {
        Page p = new PageUpdate(this.ctx, resultSet.getString("url"), resultSet.getInt("id"));
        p.setCategory(resultSet.getString("category"));
        return p;
    }

}
