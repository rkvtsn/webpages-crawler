package jCrawler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class AppFromAnotherDb extends App {

    private final String keyHtml;
    private final String keyUrl;
    private final String keyCategory;

    public AppFromAnotherDb(Connection webDb, Connection db, String query, String keyUrl, String keyHtml, String keyCategory) throws SQLException {
        super(webDb);

        Statement stmt = db.createStatement();
        this.resultSet = stmt.executeQuery(query);

        this.keyCategory = keyCategory;
        this.keyUrl = keyUrl;
        this.keyHtml = keyHtml;

    }

    @Override
    protected Page getRootPage() throws Exception {
        return new PageInsert(this.ctx, resultSet.getString(keyUrl), resultSet.getString(keyHtml), resultSet.getString(keyCategory));
    }
}
