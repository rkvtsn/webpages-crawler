package jCrawler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;


public class CrawlerByList extends AppCrawler {

    final List<String> urls;
    final String category;
    public CrawlerByList(Connection webDb, List<String> urls, String category) throws SQLException {
        super(webDb);
        this.urls = urls;
        this.category = category;
    }

    @Override
    protected List<String> getUrlList() { return this.urls; }

    @Override
    protected double getScore() {
        return 100.0;
    }

    @Override
    protected String getCategory() { return this.category; }
}
