package jCrawler;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PageInsert extends Page {

    public PageInsert(DbContext ctx, String link) throws Exception {
        super(ctx, link);
    }

    public PageInsert(DbContext ctx, String link, String htmlCode) throws Exception {
        super(ctx, link, htmlCode);
    }

    public PageInsert(DbContext ctx, String link, String htmlCode, String category) throws Exception {
        super(ctx, link, htmlCode, category);
    }


    @Override
    protected void onParseCommit() throws SQLException {
        queryParse().executeUpdate();
        ResultSet set = queryParse().getGeneratedKeys();
        set.next();
        id = set.getInt(1);
    }


    @Override
    protected PreparedStatement queryParse() {
        return ctx.getParseInsert();
    }

    @Override
    protected void onError(int statusCode) throws SQLException {
        ctx.insertTempPage(this.getPageUrl(), statusCode);
    }
}
