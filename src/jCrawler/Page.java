package jCrawler;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Page implements IPage {

    protected String category = "";

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isValid() {
        return doc == null || getPageUrl() == null;
    }

    private int _error;

    public int getError () { return _error; }

    protected abstract PreparedStatement queryParse();

    protected abstract void onError(int statusCode) throws SQLException;

    protected abstract void onParseCommit() throws SQLException;


    public int getId() { return id; }
    public int getDomainId() {
        return domainId;
    }
    public String getPageUrl() {
        return pageUrl;
    }
    public Document getDocument() {
        return doc;
    }

    protected Document doc = null;

    protected String pageUrl = null;

    protected String domain_link = null;

    protected int id = -1;

    protected int domainId = -1;

    protected DbContext ctx;


    protected Page(DbContext ctx, String link) throws Exception {
        this.ctx = ctx;
        this.domain_link = insertDomain(link);
        tryGetByHttp(this.pageUrl);
    }

    protected Page(DbContext ctx, String link, int id) throws Exception {
        this.ctx = ctx;
        this.domain_link = insertDomain(link);
        this.id = id;
        tryGetByHttp(this.pageUrl);
    }


    protected Page(DbContext ctx, String link, String htmlCode) throws Exception {
        this(ctx, link, htmlCode, "undefined", -1);
    }

    protected Page(DbContext ctx, String link, String htmlCode, int id) throws Exception {
        this(ctx, link, htmlCode, "undefined", id);
    }

    protected Page(DbContext ctx, String link, String htmlCode, String category) throws Exception {
        this(ctx, link, htmlCode, category, -1);
    }

    protected Page(DbContext ctx, String link, String htmlCode, String category, int id) throws Exception {
        this.category = category;
        this.id = id;
        this.ctx = ctx;
        this.domain_link = insertDomain(link);
        doc = Jsoup.parse(htmlCode);
    }

    private void tryGetByHttp(String link) throws InterruptedException, IOException, SQLException {

        if (domainId == -1)
            return;

        Connection.Response resp = null;
        Connection con = null;
        String url = link;

        con = Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                .referrer("http://www.google.com")
                .ignoreHttpErrors(true)
                .followRedirects(true)
                .timeout(5000);
        Thread.sleep(300); // test number

        try {
            resp = con.execute();
            if ((resp.contentType().startsWith("application") ||
                resp.contentType().startsWith("audio") ||
                resp.contentType().startsWith("video") ||
                resp.contentType().endsWith("css") ||
                resp.contentType().endsWith("javascript") ||
                resp.contentType().endsWith("csv")) && !resp.contentType().endsWith("xml"))
                    return;
            // TODO : Exception cause of @this.id
            if (resp.contentType().startsWith("image")) {
                ctx.insertImage(link, this.id);
                doc = null; // this is not html page
                return;
            }
            doc = con.get();
        } catch (Exception e) {
            badRequest(DbContext.ERROR.UNKNOWN); // unreachable or unknown error
            doc = null;
            return;
        }

        // on redirect
        int redirect_cnt = 0;
        while (resp.statusCode() >= 300 && resp.statusCode() < 400 || redirect_cnt >= 2) {
            url = resp.header("Location");
            con = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                    .referrer("http://www.google.com")
                    .ignoreHttpErrors(true)
                    .followRedirects(true)
                    .timeout(5000);
            try {
                resp = con.execute();
            } catch (Exception e) {
                badRequest(DbContext.ERROR.UNKNOWN); // unreachable or unknown error
                doc = null;
                return;
            }
        }

        if (resp.statusCode() != 200) {
            badRequest(resp.statusCode());
            doc = null;
        }

    }

    private void badRequest(int statusCode) throws SQLException {
        onError(statusCode);
        this._error = statusCode;
    }


    // link can be only abs
    protected String insertDomain(String link) throws Exception {

        String url = prepareLink(link);
        if (url == null)
            return null;

        String domain = getDomain(url);

        if (domain == null)
            return null;

        this.pageUrl = url;

        ResultSet rs = ctx.getStmt().executeQuery("SELECT id FROM domains WHERE url = '" + domain + "'");

        if (!rs.next()) {
            ctx.getStmt().execute("INSERT INTO domains (url) VALUES ('" + domain + "')", Statement.RETURN_GENERATED_KEYS);
            rs = ctx.getStmt().getGeneratedKeys();
            rs.next();
            domainId = rs.getInt(1);
        } else {
            domainId = rs.getInt("id");
        }

        return domain;
    }

    static public String getAbsLink(String link, String parentLink) throws MalformedURLException {

        String link_prepared = prepareLink(link);

        if (link_prepared == null)
            return null;

        String link_domain = getDomain(link_prepared);

        if (link_domain == null) {
            try {
                java.net.URI baseUri = new java.net.URI(parentLink);
                java.net.URI resolvedUri = baseUri.resolve(link);
                link_prepared = resolvedUri.toString();
            } catch (Exception e) {
                link_prepared = null;
            }
        }
//        else {
//            if (!domain_link.equals(link_domain)) // sibling
//                link = null; // TODO Sibling
//        }

        if (parentLink.equals(link_prepared))
            return null;

        return link_prepared;
    }

    static public String prepareLink(String link){
        if (link == null)
            return null;

        // filter link
        int hashIndex = link.indexOf("#");
        if (hashIndex != -1) {
            link = link.substring(0, hashIndex);
        }

        link = link.toLowerCase().trim();

        if (link.length() == 0||
            link.contains("javascript:") ||
            link.contains("mailto:"))
            return null;

        // decorate link
        link = link.startsWith("www.") ? link.substring(4) : link;
        link = link.replace(" ","%20");


        if (!link.startsWith("https:"))
            if (!link.startsWith("http:"))
                link = "http://" + link;

        if (!link.endsWith("/"))
            link += '/';

        return link;
    }


    static public String[] notDomain = new String[] {
            "asp", "aspx", "php", "html", "htm",
            "cgi", "pl", "js", "jsp", "shtml",
            "shtm", "cfml", "cfm", "do",
            "jpg", "jpeg", "png", "gif", "svg", "bmp",
            "mp4", "m4v",
            "doc", "docx", "rtf", "txt", "pps", "ppt",
            "csv", "xml", "flv", "psd"
    };


    // legendary method of getting domain. Java (Oracle) must learn how it must be!
    public static String getDomain(String link) {
        String domain = null;

        // get simply text between // and / that must have a dot '.'
        domain = getHost(link);
        if (domain == null)
            return null;

        // if where any query => remove it. that's for old school sites
        int queryIndex = domain.indexOf("?");
        if (queryIndex != -1) {
            domain = domain.substring(0, queryIndex);
        }

        // if domain contains script extension => that's not a domain
        for (String x : notDomain) {
            if (domain.endsWith('.' + x))
                return null;
        }

        return domain;
    }

    public static String getHost(String url) {

        int doubleSlash = url.indexOf("//");

        if(doubleSlash == -1)
            doubleSlash = 0;
        else
            doubleSlash += 2;

        int end = url.indexOf('/', doubleSlash);
        end = end >= 0 ? end : url.length();

        int port = url.indexOf(':', doubleSlash);
        end = (port > 0 && port < end) ? port : end;

        url = url.substring(doubleSlash, end);

        if (!url.contains("."))
            url = null;

        return url;
    }

    private void prepareData(PreparedStatement query) throws SQLException {
        String html_code = doc.html();
        String html_text = doc.text();

        String title = doc.getElementsByTag("title").text();
        String meta_keywords = doc.select("meta[name=keywords]").attr("content");
        String meta_description = doc.getElementsByTag("meta[name=description]").attr("content");
        String p_text = doc.select("p").text();

        // .content
        String content = doc.select("div.content, .content, div#content, article, section").text();
        // .header
        String header = doc.select("#header, .header, header").text();
        // .footer
        String footer = doc.select("#footer, .footer, footer").text();

        _error = DbContext.ERROR.OK;

        query.setString(1, html_code);
        query.setString(2, html_text);
        query.setString(3, title);
        query.setString(4, meta_keywords);
        query.setString(5, meta_description);
        query.setString(6, p_text);
        query.setString(7, content);
        query.setString(8, header);
        query.setString(9, footer);
        query.setInt(10, _error);
        query.setString(11, getPageUrl());
        query.setInt(12, getDomainId());
    }


    public List<String> parse() throws SQLException, MalformedURLException {

        if (doc == null || getPageUrl() == null) return new ArrayList<String>();

        PreparedStatement query = queryParse();

        this.prepareData(query);

        try {
            onParseCommit();
        } catch (Exception e) {
            doc = Jsoup.parse(Normalizer.normalize(doc.html(), Normalizer.Form.NFD).replaceAll("\\x00", ""));
            this.prepareData(query);
            onParseCommit();
        }
        queryParse().clearParameters();

        ctx.setAutoCommit(false);
        // @decorators em, i; cite; b, strong; u; quote; del, strike; mark; dfn
        ctx.insertTexts(doc.select("cite, quote, del, strike, mark, dfn"), "tag_decoration", this.id);
        ctx.insertTexts(doc.getElementsByTag("u"), "tag_u", this.id);
        ctx.insertTexts(doc.select("em, i"), "tag_italic", this.id);
        ctx.insertTexts(doc.select("b, strong"), "tag_bold", this.id);
        // @blocks
        ctx.insertTexts(doc.getElementsByTag("div"), "tag_div", this.id);
        ctx.insertTexts(doc.getElementsByTag("span"), "tag_span", this.id);
        // @h*
        ctx.insertTexts(doc.getElementsByTag("h1"), "tag_h1", this.id);
        ctx.insertTexts(doc.getElementsByTag("h2"), "tag_h2", this.id);
        ctx.insertTexts(doc.getElementsByTag("h3"), "tag_h3", this.id);
        // @img
        ctx.insertImages(doc.getElementsByTag("img"), this.id);
        // @nav navigation-elements
        ctx.insertNavLinks(doc.select(".nav a, #nav a, #menu a, .menu a, menu a, ul a"), this.id);
        // all @links
        List<String> childLinks = new ArrayList<String>();//= ctx.insertLinks(doc.getElementsByTag("a"), this.id);
        for (Element link : doc.getElementsByTag("a")) {
            String href = getAbsLink(link.attr("href"), this.getPageUrl());
            if (href != null) {
                childLinks.add(href);
                link.attr("href", href);
                ctx.onInsertLinkAdd(link, this.id);
            }
        }
        ctx.onInsertLinkExecute();

        ctx.setAutoCommit(true);

        // save @category
        if (getCategory() != null && getCategory() != "")
            ctx.insertPageCategory(getCategory(), this.id, 100);

        return new ArrayList<String>(new HashSet<String>(childLinks));
    }

    public List<String> parse(int parentId) throws SQLException, MalformedURLException {

        List<String> links = parse();
        ctx.insertChild(this.getId(), parentId);
        return links;

    }

}
