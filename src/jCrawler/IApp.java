package jCrawler;


public interface IApp {

    void launch() throws Exception;

    void launch(int depth) throws Exception;
}
