package jCrawler;


import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PageUpdate extends Page {

    public PageUpdate(DbContext ctx, String link, int id) throws Exception {
        super(ctx, link, id);
    }

    public PageUpdate(DbContext ctx, String link, int id, String html) throws Exception {
        super(ctx, link, html, id);
    }

    @Override
    protected PreparedStatement queryParse() {
        return ctx.getParseUpdate();
    }

    @Override
    protected void onError(int statusCode) throws SQLException {
        ctx.updateTempPage(this.id, statusCode);
    }

    @Override
    protected void onParseCommit() throws SQLException {
        queryParse().setInt(13, this.id);
        queryParse().executeUpdate();
    }
}
