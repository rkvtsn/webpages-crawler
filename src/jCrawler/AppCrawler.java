package jCrawler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public abstract class AppCrawler implements IApp {

    protected ProxyConnection proxyConnection;

    protected final Connection webDb;
    protected final DbContext ctx;
    protected ResultSet resultSet;
    private List<String> rootLinks;


    public AppCrawler(Connection webDb) throws SQLException {

        // connect to DB
        this.webDb = webDb;
        ctx = new DbContext(webDb);

    }

    @Override
    public void launch() throws Exception {
        launch(2);
    }



    @Override
    public void launch(int depth) throws Exception {

        rootLinks = getUrlList();

        if (rootLinks.size() == 0)
            throw new Exception("List of urls is empty!");

        // get queue // it takes all TEMP
        resultSet = prepareQueue();

        boolean isParent = (depth > 0);

        // insert queue
        while(resultSet.next()) {
            System.out.println("FROM LIST:");
            parse(genRootPage(), isParent);
        }


        resultSet = ctx.selectWhereError("<", 0);

        // insert queue's own
        for (int i = 0; i < depth; i++) {
            isParent = depth - i >= 1;
            while(resultSet.next()) {
                System.out.println("CHILD [" + i + "]: ");
                parse(new PageUpdate(ctx, resultSet.getString("url"), resultSet.getInt("id")), isParent);
            }
            resultSet = ctx.selectWhereError("<", 0);
        }

        System.out.println("We are done!");
        Thread.sleep(1000);
    }

    /**
     * parse
     * get page child list
     * insert child list in queue with category
     * [do not override]
     */
    protected void parse(IPage parentPage, boolean isParent) throws Exception {
        // parse Parent & get inner links
        System.out.println("\tPage on parse: [" + parentPage.getPageUrl() + "]");
        List<String> childList = parentPage.parse();
        System.out.println("\t\t STATUS: (" + parentPage.getError() + ")");

        // if we don't need childList;
        if (!isParent) return;

        Set<String> set = new LinkedHashSet<String>(childList);
        childList = new ArrayList<String>(set);

        if (childList.size() == 0)
            return;

        // insert to queue
        ctx.insertTempPages(childList, -1 * parentPage.getId());
        // get queue
        ResultSet rs = ctx.selectWhereError( -1 * parentPage.getId());

        // add category to children
        while (rs.next()) {
            ctx.insertPageCategory(getCategory(), rs.getInt("id"), getScore());
            ctx.insertChild(rs.getInt("id"), parentPage.getId());
        }
    }


    /**
     * generate root page
     * [can be override]
     * @return IPage rootPage for parsing
     */
    protected IPage genRootPage() throws Exception {
        return new PageUpdate(ctx, resultSet.getString("url"), resultSet.getInt("id"));
    }

    /**
     * insert in db urls and category then get it back
     * [can be override]
     * @return ResultSet of pages queue
     */
    protected ResultSet prepareQueue() throws SQLException {

        // insert new queue in db
        ctx.insertTempPages(rootLinks, DbContext.ERROR.TEMP);
        ResultSet rs =  ctx.selectWhereError(DbContext.ERROR.TEMP);

        // add category for queue
        while (rs.next()) {
            ctx.insertPageCategory(getCategory(), rs.getInt("id"), getScore());
        }

        return ctx.selectWhereError(DbContext.ERROR.TEMP);
    }


    /**
     * abstract method for getting category title
     * @return String
     */
    protected abstract String getCategory(); // for db2db: getString("category")


    /**
     * abstract method for getting List of Root page Urls
     * @return List<String>
     */
    protected abstract List<String> getUrlList();


    protected abstract double getScore();
}
