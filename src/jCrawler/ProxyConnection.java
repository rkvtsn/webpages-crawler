package jCrawler;


import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class ProxyConnection {

    public ProxyConnection(String host, String port) {
        proxyConnect(host, port);
    }

    public ProxyConnection(String host, String port, String login, String password) {
        this(host, port);
        proxyAuth(login, password);
    }

    private void proxyConnect(String proxyHost, String proxyPort) {
        System.setProperty("http.proxyHost", proxyHost);
        System.setProperty("http.proxyPort", proxyPort);
    }

    protected void proxyAuth(final String login, final String password) {
        Authenticator.setDefault(
                new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                login, password.toCharArray());
                    }
                }
        );
        System.setProperty("http.proxyUser", login);
        System.setProperty("http.proxyPassword", password);
    }


}
