package jCrawler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

abstract public class App implements IApp{

    public void setProxy(ProxyConnection proxyConnection) {
        // TODO
    }

    final static private int MAX_DEPTH = 3;

    protected final Connection webDb;
    protected final DbContext ctx;
    ResultSet resultSet;

    protected App(Connection webDb) throws SQLException {

        this.webDb = webDb;
        ctx = new DbContext(webDb);

    }

    public void launch() throws Exception {
        launch(MAX_DEPTH);
    }

    public void launch(int depth) throws Exception {
        while (resultSet.next()) {
            parsePage(getRootPage(), depth);
        }
        System.out.println("We are done!");
        Thread.sleep(1000);
    }

    protected void parsePage(IPage page) throws Exception {
        parsePage(page, MAX_DEPTH);
    }

    protected void parsePage(IPage page, int depth) throws Exception {
        Set<String> addedLinks = new HashSet<String>();

        System.out.println("\tPage on parse: [" + page.getPageUrl() + "]");
        List<String> links = page.parse();
        System.out.println("\tSTATUS: (" + page.getError() + ")");

        addedLinks.add(page.getPageUrl());

        if (depth < 1) return; // TODO: too many lines of same sh*t

        System.out.println("\tlink 1st group: ");
        for (String link : links) {
//            if (page.getPageUrl().equals(link)) continue; // prevent recursion
            IPage page1group = new PageInsert(ctx, link);

            if (addedLinks.contains(page1group.getPageUrl())) continue;
            addedLinks.add(page1group.getPageUrl());

            List<String> links2 = page1group.parse(page.getId());
            System.out.println(page1group.getPageUrl());

            if (depth < 2) continue;
            System.out.println("\t\tlink 2nd group: ");
            for (String link2 : links2) {
//                if (page.getPageUrl().equals(link2) || link.equals(link2)) continue; // prevent recursion
                IPage page2group = new PageInsert(ctx, link2);

                if (addedLinks.contains(page2group.getPageUrl())) continue;
                addedLinks.add(page2group.getPageUrl());

                List<String> links3 = page2group.parse(page1group.getId());
                System.out.println(page2group.getPageUrl());

                if (depth < 3) continue;

                System.out.println("\t\t\tlink 3rd group: ");
                for (String link3 : links3) {
//                    if (page.getPageUrl().equals(link2) || link.equals(link3) || link2.equals(link3)) continue; // prevent recursion
                    IPage page3group = new PageInsert(ctx, link3);

                    if (addedLinks.contains(page3group.getPageUrl())) continue;
                    addedLinks.add(page3group.getPageUrl());

                    page3group.parse(page2group.getId());
                    System.out.println(page3group.getPageUrl());

                }
            }
        }
        addedLinks.clear();
    }


    protected abstract Page getRootPage() throws Exception;

}