package jCrawler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class AppFromTxt extends App {


    public AppFromTxt(Connection webDb, String filename) throws SQLException, IOException {
        super(webDb);

        String tmp;
        List<String> links = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        while ((tmp = reader.readLine()) != null) {
            links.add(tmp);
        }
        reader.close();
        ctx.insertTempPages(links, DbContext.ERROR.TEMP);

        resultSet = ctx.selectWhereError(DbContext.ERROR.TEMP);


    }

    @Override
    protected Page getRootPage() throws Exception {
        return new PageUpdate(this.ctx, resultSet.getString("url"), resultSet.getInt("id"));
    }

    @Override
    public void launch(int depth) throws Exception {

    }


}
