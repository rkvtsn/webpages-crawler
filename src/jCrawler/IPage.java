package jCrawler;

import org.jsoup.nodes.Document;

import java.net.MalformedURLException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public interface IPage {

    int getId();
    int getDomainId();
    String getPageUrl();

    int getError();

    Document getDocument();
    List<String> parse() throws SQLException, MalformedURLException;
    List<String> parse(int parentId) throws SQLException, MalformedURLException;

    boolean isValid();

}
